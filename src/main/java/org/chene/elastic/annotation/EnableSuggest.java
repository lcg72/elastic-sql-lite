package org.chene.elastic.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.chene.elastic.enumeration.EsAnalyzer;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableSuggest {

    String suggestName() default "suggest";

    /**
     * 分词器
     * @return {@link EsAnalyzer}
     */
    EsAnalyzer analyzerType() default EsAnalyzer.not_analyzed;

}
