package org.chene.elastic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.chene.elastic.enumeration.EsAnalyzer;
import org.chene.elastic.enumeration.EsFieldType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EsField {

	/**
	 * 字段名称
	 * 
	 * @return
	 */
	public String fieldName() default "";
	
    /**
     * 字段类型,默认为string
     * 
     * @return
     */
    public EsFieldType fieldType() default EsFieldType.STRING;

	/**
	 * 分词器,默认不进行分词
	 * 
	 * @return
	 */
	EsAnalyzer analyzerType() default EsAnalyzer.not_analyzed;

	/**
	 * 是否存储,默认为是
	 * 
	 * @return
	 */
	public boolean isStore() default true;
}
