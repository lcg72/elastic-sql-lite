package org.chene.elastic.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class AnnotationUtil {
	public static boolean containsAnnotation(Class<?> target, Class<? extends Annotation> annotation) {
		return null == target.getAnnotation(annotation);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getFieldValueByAnnotation(Object obj, Class<? extends Annotation> annotation)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = obj.getClass().getDeclaredFields();

		for (Field field : fields) {
			if (null != field.getAnnotation(annotation)) {
				field.setAccessible(true);
				return (T) field.get(obj);
			}
		}

		return null;
	}

	/*	@SuppressWarnings("unchecked")
		public static <T> List<T> getFieldValuesByDeclaredClassAnnotation(Object obj, Class<? extends Annotation> annotation)
				throws IllegalArgumentException, IllegalAccessException {
			Field[] fields = obj.getClass().getDeclaredFields();
			List<T> values = new ArrayList<T>();

			for (Field field : fields) {
				if (containsAnnotation(field.getDeclaringClass(), annotation)) {
					field.setAccessible(true);
					values.add((T) field.get(obj));
				}
			}

			return values;
		}*/
}
