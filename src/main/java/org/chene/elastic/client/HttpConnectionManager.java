package org.chene.elastic.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.chene.elastic.service.IndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HttpConnectionManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndexService.class);

	private static final int CONNECT_TIMEOUT = 3000;// HttpConnectTimeout 设置连接建立的超时时间为10s
	private static final int SOCKET_TIMEOUT = 5000;// HttpSocketTimeout
	private static final int MAX_CONN = 1000; // HttpMaxPoolSize 最大连接数
	private static final int MAX_PRE_ROUTE = 1000;//
	private static final int MAX_ROUTE = 1000;//
	private static final int HTTP_MONITOR_INTERVAL = 1000;
	private static final int HTTP_IDEL_TIMEOUT = 5000;// http连接空闲时间

	private CloseableHttpClient httpClient; // 发送请求的客户端单例
	private PoolingHttpClientConnectionManager manager; // 连接池管理类
	private ScheduledExecutorService monitorExecutor;

	private String hosts;
	private Integer port;
	private String httpGetURL;

	private RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(CONNECT_TIMEOUT)
			.setConnectTimeout(CONNECT_TIMEOUT).setSocketTimeout(SOCKET_TIMEOUT).build();

	public HttpConnectionManager(String hosts, Integer port) {
		this.hosts = hosts;
		this.port = port;
		this.httpGetURL = "http://" + hosts + ":" + port;

		httpClient = createHttpClient(hosts, port);

		// 开启监控线程,对异常和空闲线程进行关闭
		monitorExecutor = Executors.newScheduledThreadPool(1);
		monitorExecutor.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				// 关闭异常连接
				manager.closeExpiredConnections();
				// 关闭5s空闲的连接
				manager.closeIdleConnections(HTTP_IDEL_TIMEOUT, TimeUnit.MILLISECONDS);
				LOGGER.info("close expired and idle for over 5s connection");
			}
		}, HTTP_MONITOR_INTERVAL, HTTP_MONITOR_INTERVAL, TimeUnit.MILLISECONDS);
	}

	/**
	 * 根据host和port构建httpclient实例
	 * 
	 * @param host
	 *            要访问的域名
	 * @param port
	 *            要访问的端口
	 * @return
	 */
	public CloseableHttpClient createHttpClient(String host, int port) {
		ConnectionSocketFactory plainSocketFactory = PlainConnectionSocketFactory.getSocketFactory();
		LayeredConnectionSocketFactory sslSocketFactory = SSLConnectionSocketFactory.getSocketFactory();
		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
				.register("http", plainSocketFactory).register("https", sslSocketFactory).build();

		manager = new PoolingHttpClientConnectionManager(registry);
		// 设置连接参数
		manager.setMaxTotal(MAX_CONN); // 最大连接数
		manager.setDefaultMaxPerRoute(MAX_PRE_ROUTE); // 路由最大连接数

		HttpHost httpHost = new HttpHost(host, port);
		manager.setMaxPerRoute(new HttpRoute(httpHost), MAX_ROUTE);

		// 请求失败时,进行请求重试
		HttpRequestRetryHandler handler = new HttpRequestRetryHandler() {
			@Override
			public boolean retryRequest(IOException e, int i, HttpContext httpContext) {
				if (i > 3) {
					// 重试超过3次,放弃请求
					LOGGER.error("retry has more than 3 time, give up request");
					return false;
				}
				if (e instanceof NoHttpResponseException) {
					// 服务器没有响应,可能是服务器断开了连接,应该重试
					LOGGER.error("receive no response from server, retry");
					return true;
				}
				if (e instanceof SSLHandshakeException) {
					// SSL握手异常
					LOGGER.error("SSL hand shake exception");
					return false;
				}
				if (e instanceof InterruptedIOException) {
					// 超时
					LOGGER.error("InterruptedIOException");
					return false;
				}
				if (e instanceof UnknownHostException) {
					// 服务器不可达
					LOGGER.error("server host unknown");
					return false;
				}
				if (e instanceof ConnectTimeoutException) {
					// 连接超时
					LOGGER.error("Connection Time out");
					return false;
				}
				if (e instanceof SSLException) {
					LOGGER.error("SSLException");
					return false;
				}

				HttpClientContext context = HttpClientContext.adapt(httpContext);
				HttpRequest request = context.getRequest();
				if (!(request instanceof HttpEntityEnclosingRequest)) {
					// 如果请求不是关闭连接的请求
					return true;
				}
				return false;
			}
		};

		CloseableHttpClient client = HttpClients.custom().setConnectionManager(manager).setRetryHandler(handler)
				.build();
		return client;
	}

	public String get(String url) {
		HttpGet httpGet = new HttpGet(httpGetURL + url);
		httpGet.setConfig(requestConfig);

		CloseableHttpResponse response = null;
		InputStream in = null;
		try {
			response = httpClient.execute(httpGet, HttpClientContext.create());
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				return EntityUtils.toString(entity, "UTF-8");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
				if (response != null)
					response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String post(String url, Map<String, String> params) {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(requestConfig);

		setPostParams(httpPost, params);
		CloseableHttpResponse response = null;
		InputStream in = null;
		try {
			response = httpClient.execute(httpPost, HttpClientContext.create());
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				return EntityUtils.toString(entity, "UTF-8");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
				if (response != null)
					response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 设置post请求的参数
	 * 
	 * @param httpPost
	 * @param params
	 */
	private void setPostParams(HttpPost httpPost, Map<String, String> params) {
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		Set<String> keys = params.keySet();
		for (String key : keys) {
			nvps.add(new BasicNameValuePair(key, params.get(key)));
		}
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 关闭连接池
	 */
	public void closeConnectionPool() {
		try {
			httpClient.close();
			manager.close();
			monitorExecutor.shutdown();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getHosts() {
		return hosts;
	}

	public Integer getPort() {
		return port;
	}
}
