package org.chene.elastic.client;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Service;

@Service
public class EsClientManager {

	/**
	 * client
	 */
	private TransportClient client;
	
	private String hosts;
	
	private String clusterName;

	/**
	 * 根据es 主机地址设置client
	 * 
	 * @param hosts
	 *            主机地址字串
	 */
	public EsClientManager(String hosts, String clusterName) {
		if (hosts == null || hosts.split(",").length == 0)
			throw new NullPointerException("es服务器地址空...");
		if (clusterName == null || "".equals(clusterName))
			throw new NullPointerException("es集群名称为空...");
		
		this.hosts = hosts;
		this.clusterName = clusterName;

		Settings settings = ImmutableSettings.settingsBuilder().put("cluster.name", clusterName)
				.put("client.transport.sniff", true).build();
		client = new TransportClient(settings);
		// 分割成地址数组
		String[] hostArr = hosts.split(",");
		/* 添加 */
		for (String host : hostArr) {
			client.addTransportAddress(new InetSocketTransportAddress(host, 9300));
		}
	}

	/**
	 * 获得client
	 *
	 * @return {@link TransportClient}
	 */
	public TransportClient getClient() {
		return client;
	}

	public String getHosts() {
		return hosts;
	}

	public String getClusterName() {
		return clusterName;
	}

}
