package org.chene.elastic.bean;

import java.io.Serializable;
import java.util.Map;

public class UpdatableDocument implements Serializable {
	private static final long serialVersionUID = 5098519405490149844L;

	private Class<?> docType;

	private String id;

	private Map<String, Object> values;

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Class<?> getDocType() {
		return docType;
	}

	public void setDocType(Class<?> docType) {
		this.docType = docType;
	}
}
