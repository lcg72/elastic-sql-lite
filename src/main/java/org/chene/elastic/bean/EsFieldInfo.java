package org.chene.elastic.bean;

import java.lang.reflect.Field;

import org.chene.elastic.annotation.EsField;

import com.esotericsoftware.reflectasm.MethodAccess;

public class EsFieldInfo {
	private String fieldName;

	private Field target;

	private Class<?> targetClazz;

	private EsField esField;

	private int index;

	private MethodAccess targetMethodAccess;

	public EsField getEsField() {
		return esField;
	}

	public void setEsField(EsField esField) {
		this.esField = esField;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public MethodAccess getTargetMethodAccess() {
		return targetMethodAccess;
	}

	public void setTargetMethodAccess(MethodAccess targetMethodAccess) {
		this.targetMethodAccess = targetMethodAccess;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Field getTarget() {
		return target;
	}

	public void setTarget(Field target) {
		this.target = target;
	}

	public Class<?> getTargetClazz() {
		return targetClazz;
	}

	public void setTargetClazz(Class<?> targetClazz) {
		this.targetClazz = targetClazz;
	}

}
