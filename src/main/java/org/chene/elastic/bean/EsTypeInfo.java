package org.chene.elastic.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.esotericsoftware.reflectasm.MethodAccess;

public class EsTypeInfo {
	private Class<?> target;

	private int index;

	private MethodAccess methodAccess;

	private List<EsFieldInfo> basicFields = new ArrayList<EsFieldInfo>();

	private Map<EsFieldInfo, EsTypeInfo> beanFields = new HashMap<EsFieldInfo, EsTypeInfo>();

	public Class<?> getTarget() {
		return target;
	}

	public void setTarget(Class<?> target) {
		this.target = target;
	}

	public MethodAccess getMethodAccess() {
		return methodAccess;
	}

	public void setMethodAccess(MethodAccess methodAccess) {
		this.methodAccess = methodAccess;
	}

	public void add(EsFieldInfo esFieldInfo) {
		this.basicFields.add(esFieldInfo);
	}

	public void add(EsFieldInfo esFieldInfo, EsTypeInfo beanFieldEsTypeInfo) {
		this.beanFields.put(esFieldInfo, beanFieldEsTypeInfo);
	}

	public List<EsFieldInfo> getBasicFields() {
		return basicFields;
	}

	public void setBasicFields(List<EsFieldInfo> basicFields) {
		this.basicFields = basicFields;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Map<EsFieldInfo, EsTypeInfo> getBeanFields() {
		return beanFields;
	}

	public void setBeanFields(Map<EsFieldInfo, EsTypeInfo> beanFields) {
		this.beanFields = beanFields;
	}
}
