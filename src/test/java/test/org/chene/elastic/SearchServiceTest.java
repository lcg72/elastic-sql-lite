package test.org.chene.elastic;

import java.util.List;

import org.chene.elastic.client.EsClientManager;
import org.chene.elastic.client.HttpConnectionManager;
import org.chene.elastic.service.IndexService;
import org.chene.elastic.service.SearchService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.fastjson.JSON;

@Configuration
public class SearchServiceTest {
	private static final String INDEX = "test-index";

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext context = new AnnotationConfigApplicationContext(SearchServiceTest.class);
		SearchService searchService = context.getBean(SearchService.class);

		// see https://github.com/NLPchina/elasticsearch-sql
		List<UserInfo> userList = searchService.list(INDEX, UserInfo.class,
				"select * from test-index/UserInfo where userName=matchQuery('张三')");
		System.out.println(JSON.toJSONString(userList));

		context.getBean(HttpConnectionManager.class).closeConnectionPool();
	}

	@Bean
	public IndexService getIndexService() {
		return new IndexService();
	}

	@Bean
	public SearchService getSearchService() {
		return new SearchService();
	}

	@Bean
	public EsClientManager getESClientManager() {
		return new EsClientManager("192.168.3.235", "elasticsearch");
	}

	@Bean
	public HttpConnectionManager getHttpConnectionManager() {
		return new HttpConnectionManager("192.168.3.235", 9200);
	}
}
