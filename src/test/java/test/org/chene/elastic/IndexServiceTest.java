package test.org.chene.elastic;

import org.chene.elastic.client.EsClientManager;
import org.chene.elastic.client.HttpConnectionManager;
import org.chene.elastic.service.IndexService;
import org.chene.elastic.service.SearchService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IndexServiceTest {
	private static final String INDEX = "test-index";

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(123l);
		userInfo.setUserName("张三");
		userInfo.setAge(28);

		UserExtInfo userExtInfo = new UserExtInfo();
		userExtInfo.setId(111l);
		userExtInfo.setUserId(userInfo.getUserId());
		userExtInfo.setAddress("湖南省长沙市岳麓区企业广场");
		userExtInfo.setMoney(101.02);
		userInfo.setUserExtInfo(userExtInfo);

		ApplicationContext context = new AnnotationConfigApplicationContext(IndexServiceTest.class);
		IndexService indexService = context.getBean(IndexService.class);

		// indexService.createIndex(INDEX);
		// indexService.deleteIndex(INDEX);
		// System.out.println(indexService.isIndexExists(INDEX));

		// System.out.println(indexService.isTypeExist(INDEX, UserInfo.class.getSimpleName()));

		indexService.save(INDEX, userInfo, userExtInfo);
		// indexService.delete(INDEX, userExtInfo);
		
		context.getBean(HttpConnectionManager.class).closeConnectionPool();
	}

	@Bean
	public IndexService getIndexService() {
		return new IndexService();
	}

	@Bean
	public SearchService getSearchService() {
		return new SearchService();
	}

	@Bean
	public EsClientManager getESClientManager() {
		return new EsClientManager("192.168.3.235", "elasticsearch");
	}

	@Bean
	public HttpConnectionManager getHttpConnectionManager() {
		return new HttpConnectionManager("192.168.3.235", 9200);
	}
}
