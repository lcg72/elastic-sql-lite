package test.org.chene.elastic;

import org.chene.elastic.annotation.EsField;
import org.chene.elastic.annotation.EsKey;
import org.chene.elastic.enumeration.EsAnalyzer;
import org.chene.elastic.enumeration.EsFieldType;

public class UserInfo {

	@EsKey
	@EsField(fieldType = EsFieldType.LONG)
	private Long userId;

	@EsField(analyzerType = EsAnalyzer.ik)
	private String userName;
	
	private int age;
	
	private UserExtInfo userExtInfo;

	public UserInfo() {
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public UserExtInfo getUserExtInfo() {
		return userExtInfo;
	}

	public void setUserExtInfo(UserExtInfo userExtInfo) {
		this.userExtInfo = userExtInfo;
	}
}
