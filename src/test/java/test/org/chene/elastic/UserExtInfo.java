package test.org.chene.elastic;

import org.chene.elastic.annotation.EsField;
import org.chene.elastic.annotation.EsKey;
import org.chene.elastic.enumeration.EsAnalyzer;
import org.chene.elastic.enumeration.EsFieldType;

public class UserExtInfo {
	@EsKey
	@EsField(fieldType = EsFieldType.LONG)
	private Long id;

	@EsField(fieldType = EsFieldType.LONG)
	private Long userId;

	@EsField(analyzerType = EsAnalyzer.ik)
	private String address;

	private double money;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}
}
